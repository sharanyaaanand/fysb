<style type="text/css">
body {
  background-color: #f1f1f1;
  font-family: Arial, Helvetica, sans-serif;
  margin-top: 50px;
}

td {
  vertical-align: middle !important;
}

.taxonomy {
  color: #ee8941;
  font-family: Arial, Helvetica, sans-serif;
  font-style: italic;
}

.taxonomy-line {
  color: #999999;
  font-family: Arial, Helvetica, sans-serif;
}

.date-display-single {
  color: #ee8941;
}

.blurb {
  color: #999999;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 12px;
  line-height: 16px;
  margin-top: 10px;
}

a {
  line-height: 19px;
}

.blurb a {
  color: #ee8941;
  text-decoration: none;
}

</style>

<!-- HEADER -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#F1F1F1" height="100%">
<tr>
<td>

<table width="650" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#F1F1F1" style="margin:0 auto;">
  <tr>
    <td valign="top">
    <table width="650" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#F1F1F1">
    <tr><td><img src="http://www2.jbsinternational.com/dc/email/ncfy/images/header.jpg" width="650" height="120" /></td></tr>
    </table>

    <table width="650" border="0" cellpadding="0" cellspacing="0" align="center">
    <tr><td>&nbsp;</td></tr></table>
    
    <table width="650" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#FFFFFF">
    <tr>
    <td valign="top" width="334" style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; line-height: 22px; color: #000000; padding-top: 15px; padding-left: 20px;">
      <?php print $featuretext; ?>
    </td>
    <td width="32" valign="top" align="right">&nbsp;</td>
    <td valign="top" align="right"><?php print $featureimage; ?></td>
    </tr>
    </table>

    <table width="650" border="0" cellpadding="0" cellspacing="0" align="center">
    <tr><td>&nbsp;</td></tr></table>

<!-- content -->
<?php print $newsarea; ?>
<!-- /content -->

    <table width="650" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#DDE8EE">
    <tr>
    <td valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 19px; color: #707070; padding: 25px;">
    <p><span style="color: #2d261a;">Funding Opportunities</span> <span style="color: #999999;">&mdash;&mdash;&mdash;&mdash;</span></p>

<!-- content -->
<?php print $funding; ?>
<!-- /content -->

    </td>
    </tr>
    <tr>
    <td valign="top" align="center"><img src="http://www2.jbsinternational.com/dc/email/ncfy/images/social.png" width="610" height="33" border="0" usemap="#Map" /><map name="Map" id="Map">
  <area shape="rect" coords="296,2,318,27" href="http://twitter.com/ncfy" target="_blank" />
  <area shape="rect" coords="323,2,343,27" href="http://www.facebook.com/NationalClearinghouseonFamiliesandYouth" target="_blank" />
  <area shape="rect" coords="347,2,375,27" href="https://www.youtube.com/user/usgovACF" target="_blank" />
</map></td>
    </tr>
    </table>

    <table width="650" border="0" cellpadding="0" cellspacing="0" align="center">
    <tr><td>&nbsp;</td></tr></table>

    <table width="650" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#DDE8EE">
    <tr>
    <td valign="top" style="font-family: Arial, Helvetica, sans-serif; color: #707070; font-size: 14px; padding: 25px;">
    <p><span style="color: #2d261a; line-height: 19px;">Are you on our e-mail list?</span> <span style="color: #999999;">&mdash;&mdash;&mdash;&mdash;</span></p>

<p style="margin-bottom: 0;">If someone forwarded this newsletter to you, <a href="http://ncfy.acf.hhs.gov/subscribe">please join our mailing list</a>.</p>

    </td>
    </tr>
    </table>
    
    <table width="650" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#DDE8EE">
    <tr><td>&nbsp;</td></tr></table>
    
     <table width="650" border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
    <td valign="center" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 15px; color: #707070; padding: 25px;"><a href="http://www.acf.hhs.gov/disclaimers" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #707070; text-decoration:none;">Disclaimer</a> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; Questions or problems? Email us at <a href="mailto:ncfy@acf.hhs.gov" style="color: #707070;">ncfy@acf.hhs.gov</a></td>
    </tr>
    </table>

</td></tr></table>

</td></tr></table>
