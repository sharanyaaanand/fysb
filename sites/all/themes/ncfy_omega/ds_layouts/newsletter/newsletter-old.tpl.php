<!-- HEADER -->
<table class="head-wrap" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;border-spacing: 0;width: 100%;">
  <tr style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
    <td style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></td>
    <td class="header container" align="" style="margin: 0 auto;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;display: block;max-width: 760px;clear: both;">
      
      <!-- /content -->
      <div class="content" style="margin: 0 auto;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;max-width: 660px;display: block;">
        <table bgcolor="#7493ae" border="0" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;border-spacing: 0;width: 100%;">
          <tr style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
            <td style="vertical-align:middle; margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"><img src="http://ncfy.acf.hhs.gov/sites/all/themes/ncfy_omega/images/email-banner.png" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;max-width: 100%;"></td>
            <td align="center" width="100" style="vertical-align:middle; margin: 0;padding: 0 20px 0 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"><h6 class="collapse" style="margin: 0;padding: 0;font-family: &quot;HelveticaNeue-Light&quot;, &quot;Helvetica Neue Light&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif;line-height: 1.1;margin-bottom: 10px;color: #FFF;font-weight: 600;font-size: 14px;">
              <?php print $date; ?></h6></td>
          </tr>
        </table>
      </div><!-- /content -->
      
    </td>
    <td style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></td>
  </tr>
</table><!-- /HEADER -->

<!-- BODY -->
<table class="body-wrap" bgcolor="" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;border-spacing: 0;width: 100%;">
  <tr style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
    <td style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></td>
    <td class="container" align="" bgcolor="#FFFFFF" style="margin: 0 auto;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;display: block;max-width: 660px;clear: both;">
      
      <!-- content -->
      <div class="content" style="margin: 0 auto;padding: 15px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;max-width: 760px;display: block;">
        <table bgcolor="" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;border-spacing: 0;width: 100%;">
          <tr style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
            <td class="small" width="50%" style="vertical-align: top;margin: 0;padding: 0;padding-right: 10px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"><?php print $featureimage; ?></td>
            <td style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
              <h1 style="margin: 0;padding: 0;font-family: &quot;HelveticaNeue-Light&quot;, &quot;Helvetica Neue Light&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif;line-height: 1.1;margin-bottom: 10px;color: #000;font-weight: 500;font-size: 24px;"><?php print $featuretext; ?></h1>
            </td>
          </tr>
        </table>
      </div><!-- /content -->
      
      <!-- content -->
      <div class="content" style="margin: 0 auto;padding: 0 15px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;max-width: 760px;display: block; width: 100%;">
<?php print $newsarea; ?></div><!-- /content -->
      
      <!-- content -->
      <div class="content" style="margin: 0 auto; padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;max-width: 760px;display: block;">
        <table class="callout" style="float: left; margin: 30px 0 0 0; padding: 15px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;border-spacing: 0;background-color: #eeeae3;margin-bottom: 15px;width: 100%;">
        <tr style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
          <td style="margin:0; padding: 15px; font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
<?php print $funding; ?>
          </td>
        </tr>
      </table></div><!-- /content -->

      <!-- content -->
      <div class="content footer" style="float: left; margin: 0 auto;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;max-width: 660px;display: block;">
        <h2 style="margin: 0;padding: 0;font-family: Verdana, Geneva, sans-serif;line-height: 1.1;margin-bottom: 10px;color: #264a64;font-weight: 200;font-size: 11px;border-bottom: 0;padding-bottom: 5px;text-transform: uppercase;">Follow Us</h2>
        <table bgcolor="" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;border-spacing: 0;width: 100%;">
        <tr style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
          <td style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
            <p style="margin: 0;padding: 0;font-family: Verdana, Geneva, sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 11px;line-height: 1.4;color: #909090;"><img src="http://www2.jbsinternational.com/dc/files/ncfy/social.jpg" border="0" usemap="#Map" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;max-width: 100%;"></p>
            <p style="margin: 0;padding: 0;font-family: Verdana, Geneva, sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 11px;line-height: 1.4;color: #909090;">
              <a href="http://www.acf.hhs.gov/disclaimers" style="margin: 0;padding: 0;font-family: Verdana, Geneva, sans-serif;color: #909090;text-decoration: none;font-size: 11px;">Disclaimer</a> |
              <a href="#" style="margin: 0;padding: 0;font-family: Verdana, Geneva, sans-serif;color: #909090;text-decoration: none;font-size: 11px;">Unsubscribe</a> | 
              Questions or problems? Email us at <a href="mailto:ncfy@acf.hhs.gov" style="margin: 0;padding: 0;font-family: Verdana, Geneva, sans-serif;color: #909090;text-decoration: none;font-size: 11px;">ncfy@acf.hhs.gov</a>
            </p>
        
          </td>
        </tr>
      </table></div><!-- /content -->
      

    </td>
    <td style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></td>
  </tr>
</table><!-- /BODY -->


<map name="Map" id="Map" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
  <area shape="rect" coords="0,0,33,31" href="http://www.facebook.com/NationalClearinghouseonFamiliesandYouth" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></area>
  <area shape="rect" coords="48,0,81,31" href="http://twitter.com/ncfy" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></area>
  <area shape="rect" coords="98,0,130,31" href="http://www.youtube.com/user/USGOVHHS" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></area>
  <area shape="rect" coords="146,0,180,31" href="http://ncfy.acf.hhs.gov/rss" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></area>
</map>


