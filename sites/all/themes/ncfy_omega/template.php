<?php

/**
 * Adds Zendesk code to every page of the site.
 * To use the standalone version, use 'zendesk-chat-standalone.js' instead.
 */
drupal_add_js('sites/all/themes/ncfy_omega/js/zendesk-chat.js');

/**
 * Implements hook_preprocess_views_view().
 * For the pages listing nominees for each genre, translate the title
 */
function ncfy_omega_preprocess_views_view(&$vars) {
  $view = &$vars['view'];
  
  // Only work on the duplicates_titles_in_the_nodeview
  if ($view->name == 'duplicates_titles_in_the_node') {
    //dpm($view);
    foreach($view->result as $key => $value)
    {
	$ids[]=$view->result[$key]->field_field_library_accession_number[0]['raw']['value'];
    }
    //dpm($ids);

    //turn access_num into an argument string (separated by + sign)
    //and store the string in $GLOBALS['VIEW_NAME_access_nums'], so it will be accessible elswhere on the site
    $GLOBALS[$view->name.'_ids']=implode('+',$ids);
/*
    // Check the term ID from the URL
    $tid = $view->args[0];
    if ($tid > 0) {
      $term = taxonomy_term_load($tid);
      $translated = i18n_taxonomy_term_name($term);
      dsm($translated);
      drupal_set_title($translated);
    }
*/
  }
}



/**
 * Theme function for videos.
 */
function ncfy_omega_youtube_video($variables) {
    $id = $variables['video_id'];
    $size = $variables['size'];
    $width = array_key_exists('width', $variables)? $variables['width'] : NULL;
    $height = array_key_exists('height', $variables)? $variables['height'] : NULL;
    $autoplay = array_key_exists('autoplay', $variables)? $variables['autoplay'] : FALSE;

    // Get YouTube settings.
    $suggest = variable_get('youtube_suggest', TRUE);
    $privacy = variable_get('youtube_privacy', FALSE);
    $wmode = variable_get('youtube_wmode', TRUE);
    $dimensions = youtube_get_dimensions($size, $width, $height);

    // Protocol changes based on current page TODO.
    $protocol = (isset($_SERVER['HTTPS']) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') ? 'https' : 'http';

    // Query string changes based on setings.
    $query = array();
    if (!$suggest) {
        $query['rel'] = '0';
    }
    if ($wmode) {
        $query['wmode'] = 'opaque';
    }
    if ($autoplay) {
        $query['autoplay'] = '1';
    }

    // Domain changes based on settings.
    $domain = ($privacy) ? 'youtube-nocookie.com' : 'youtube.com';

    $path = $protocol . '://www.' . $domain . '/embed/' . $id;
    $src = url($path, array('query' => $query));

    $xmlData = simplexml_load_string(file_get_contents("$protocol://gdata.youtube.com/feeds/api/videos/{$id}?fields=title"));
    $title = (string)$xmlData->title;

    $output = '<iframe title="' . $title . '" width="' . $dimensions['width'] . '"
    height="' . $dimensions['height'] . '" src="' . $src . '"
    frameborder="0" allowfullscreen></iframe>';

    return $output;
}

