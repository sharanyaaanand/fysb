<?php
/**
 * Created by PhpStorm.
 * User: rmachado
 * Date: 8/15/14
 * Time: 11:42 AM
 */

function custom_solr_filters_admin_settings() {

  $environments_machine_names = array_keys(apachesolr_load_all_environments());

  $form = array();

  $form['custom_solr_filters_environment'] = array(
    '#type' => 'select',
    '#title' => t('Solr environment for Library content type'),
    '#options' => drupal_map_assoc($environments_machine_names),
    '#default_value' => variable_get('custom_solr_filters_environment', FALSE),
  );

  $form = system_settings_form($form);
  return $form;

}
