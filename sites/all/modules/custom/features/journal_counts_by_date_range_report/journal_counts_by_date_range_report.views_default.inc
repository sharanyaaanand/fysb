<?php
/**
 * @file
 * journal_counts_by_date_range_report.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function journal_counts_by_date_range_report_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'count_journals_by_date_range';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Count Journals by Date Range';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Count Journals by Date Range';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_library_journal_name' => 'field_library_journal_name',
    'nid' => 'nid',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'field_library_journal_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
      'responsive' => '',
    ),
    'nid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
      'responsive' => '',
    ),
  );
  /* Header: Global: Result summary */
  $handler->display->display_options['header']['result']['id'] = 'result';
  $handler->display->display_options['header']['result']['table'] = 'views';
  $handler->display->display_options['header']['result']['field'] = 'result';
  /* Relationship: Content: Taxonomy terms on node */
  $handler->display->display_options['relationships']['term_node_tid']['id'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['table'] = 'node';
  $handler->display->display_options['relationships']['term_node_tid']['field'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['vocabularies'] = array(
    'journals' => 'journals',
    'vocabulary_10' => 0,
    'vocabulary_3' => 0,
    'vocabulary_2' => 0,
    'vocabulary_5' => 0,
    'youth_database_taxonomy' => 0,
    'feature_type' => 0,
    'views_rss_itunes_category' => 0,
    'bayp_course' => 0,
    'start_a_youth_program' => 0,
  );
  /* Field: Content: Journal Name */
  $handler->display->display_options['fields']['field_library_journal_name']['id'] = 'field_library_journal_name';
  $handler->display->display_options['fields']['field_library_journal_name']['table'] = 'field_data_field_library_journal_name';
  $handler->display->display_options['fields']['field_library_journal_name']['field'] = 'field_library_journal_name';
  $handler->display->display_options['fields']['field_library_journal_name']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_library_journal_name']['group_columns'] = array(
    'tid' => 'tid',
  );
  /* Field: COUNT(Content: Nid) */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['group_type'] = 'count';
  $handler->display->display_options['fields']['nid']['label'] = 'COUNT';
  /* Sort criterion: Taxonomy term: Name */
  $handler->display->display_options['sorts']['name']['id'] = 'name';
  $handler->display->display_options['sorts']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['name']['field'] = 'name';
  $handler->display->display_options['sorts']['name']['relationship'] = 'term_node_tid';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'library' => 'library',
  );
  /* Filter criterion: Content: Type (field_library_description) */
  $handler->display->display_options['filters']['field_library_description_value']['id'] = 'field_library_description_value';
  $handler->display->display_options['filters']['field_library_description_value']['table'] = 'field_data_field_library_description';
  $handler->display->display_options['filters']['field_library_description_value']['field'] = 'field_library_description_value';
  $handler->display->display_options['filters']['field_library_description_value']['value'] = array(
    'Journal Article' => 'Journal Article',
  );
  /* Filter criterion: Content: Post date */
  $handler->display->display_options['filters']['created']['id'] = 'created';
  $handler->display->display_options['filters']['created']['table'] = 'node';
  $handler->display->display_options['filters']['created']['field'] = 'created';
  $handler->display->display_options['filters']['created']['operator'] = 'between';
  $handler->display->display_options['filters']['created']['value']['min'] = '10/01/2014';
  $handler->display->display_options['filters']['created']['value']['max'] = '09/30/2015';
  $handler->display->display_options['filters']['created']['exposed'] = TRUE;
  $handler->display->display_options['filters']['created']['expose']['operator_id'] = 'created_op';
  $handler->display->display_options['filters']['created']['expose']['label'] = 'Post date';
  $handler->display->display_options['filters']['created']['expose']['operator'] = 'created_op';
  $handler->display->display_options['filters']['created']['expose']['identifier'] = 'created';
  $handler->display->display_options['filters']['created']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    5 => 0,
    6 => 0,
    4 => 0,
    3 => 0,
    7 => 0,
    8 => 0,
  );
  /* Filter criterion: Content: Journal Name (field_library_journal_name) */
  $handler->display->display_options['filters']['field_library_journal_name_tid']['id'] = 'field_library_journal_name_tid';
  $handler->display->display_options['filters']['field_library_journal_name_tid']['table'] = 'field_data_field_library_journal_name';
  $handler->display->display_options['filters']['field_library_journal_name_tid']['field'] = 'field_library_journal_name_tid';
  $handler->display->display_options['filters']['field_library_journal_name_tid']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_library_journal_name_tid']['value'] = '';
  $handler->display->display_options['filters']['field_library_journal_name_tid']['vocabulary'] = 'journals';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/count-journals-by-date-range';

  /* Display: Data export */
  $handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['style_options']['provide_file'] = 1;
  $handler->display->display_options['style_options']['parent_sort'] = 0;
  $handler->display->display_options['style_options']['quote'] = 1;
  $handler->display->display_options['style_options']['trim'] = 0;
  $handler->display->display_options['style_options']['replace_newlines'] = 0;
  $handler->display->display_options['style_options']['header'] = 1;
  $handler->display->display_options['path'] = 'admin/count-journals-export';
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
    'default' => 0,
  );
  $export['count_journals_by_date_range'] = $view;

  return $export;
}
