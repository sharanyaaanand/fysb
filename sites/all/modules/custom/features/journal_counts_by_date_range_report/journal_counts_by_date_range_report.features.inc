<?php
/**
 * @file
 * journal_counts_by_date_range_report.features.inc
 */

/**
 * Implements hook_views_api().
 */
function journal_counts_by_date_range_report_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
