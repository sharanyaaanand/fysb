<?php
/**
 * Created by PhpStorm.
 * User: rmachado
 * Date: 9/17/14
 * Time: 1:41 PM
 */

class JBSApacheSolrGetResultsInfoFromNode extends JBSApacheSolrGetResultsInfoFromSolr {

  public function getResultsInfo() {
    if (!$this->resultsInfo) {
      return FALSE;
    }
    $rows = array();
    foreach ($this->resultsInfo->solrResults as $result) {
      if ($node = node_load($result['node']->entity_id)) {
        $rows[] = $this->buildRow($node);
      }
    }
    return $rows;
  }

  protected function buildRow($node) {
    $fields = array();
    foreach($this->resultsInfo->outputFields as $output_info) {
      if ($output_info['name']=='title') {
        $fields[] = $this->titleLinkToContent($node->title, 'node/' . $node->nid) ;
      }
      elseif (substr($output_info['name'],0,6)=='field_') {
        if ($output_info['callback']) {
          $fields[] = $output_info['callback']($this->getAllUnd($node->$output_info['name']));
        }
        else {
          $fields[] = $this->getAllUnd($node->$output_info['name']);
        }
      }
      else {
        $fields[] = $node->$output_info['name'];
      }
    }
    return $fields;
  }

  private function getAllUnd($field) {
    $result = '';
    for ($i=0; $i<=count($field['und'])-1; $i++) {
      $result .= (($i>0) ? ', ' : '') . $field['und'][$i]['value'];
    }
    return $result;
  }

} 