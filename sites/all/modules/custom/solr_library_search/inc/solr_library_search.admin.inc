<?php
/**
 * Created by PhpStorm.
 * User: rmachado
 * Date: 9/25/14
 * Time: 1:17 PM
 */

function solr_library_search_admin_settings() {

  $environments_machine_names = array(FALSE => '<not configured yet>') + drupal_map_assoc(array_keys(apachesolr_load_all_environments()));

  $form = array();

  $form['sls_fields_1'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings for Library Search'),
  );
  $form['sls_fields_1']['solr_library_search_environment'] = array(
    '#type' => 'select',
    '#title' => t('Solr environment'),
    '#options' => $environments_machine_names,
    '#default_value' => variable_get('solr_library_search_environment', FALSE),
  );

  return system_settings_form($form);;

}