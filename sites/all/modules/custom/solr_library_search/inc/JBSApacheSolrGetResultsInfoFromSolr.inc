<?php
/**
 * Created by PhpStorm.
 * User: rmachado
 * Date: 9/22/14
 * Time: 8:48 AM
 */

class JBSApacheSolrGetResultsInfoFromSolr  {

  protected $resultsInfo = FALSE;
  public $titleLinkClass = FALSE;

  public function __construct($get_results_info = FALSE) {
    if ($get_results_info) {
      $this->resultsInfo = $get_results_info;
    }
  }

  public function getResultsInfo() {
    if (!$this->resultsInfo) {
      return FALSE;
    }
    $rows = array();
    foreach ($this->resultsInfo->solrResults as $result) {
        $rows[] = $this->buildRow($result);
    }
    return $rows;
  }

  protected function buildRow($record) {
    $fields = array();
    foreach($this->resultsInfo->outputFields as $output_info) {
      if ($output_info['name']=='title') {
        $fields[] = $this->titleLinkToContent($record['title'], $record['link']) ;
      }
      elseif (substr($output_info['name'],0,6)=='field_') {
        $field = $record['fields'][$output_info['index']];
        if (is_array($field)) {
          $fields[] = implode(', ', $this->checkCallback($output_info['callback'], $field));
        }
        else {
          $fields[] = $this->checkCallback($output_info['callback'], $field);
        }
      }
      else {
        $fields[] = $this->checkCallback($output_info['callback'], $record[$output_info['name']]);
      }
    }
    return $fields;
  }


  protected function checkCallback($callback, $field) {
    if ($callback && function_exists($callback)) {
      if (is_array($field)) {
        foreach($field as &$value) {
          $value = $callback($value);
        }
      }
      else {
        $field =  $callback($field);
      }
    }
    return $field;
  }

  protected function titleLinkToContent($title, $link) {
    $html = '';
    if ($this->titleLinkClass) {
      $html .= '<a class="'. $this->titleLinkClass . '"';
    }
    else {
      $html .= '<a';
    }
    $html .= ' href="'. $link .'">' . $title . '</a>';
    return $html;
  }


} 