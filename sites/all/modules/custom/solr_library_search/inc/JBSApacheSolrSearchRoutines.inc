<?php
/**
 * Created by PhpStorm.
 * User: rmachado
 * Date: 9/16/14
 * Time: 1:09 PM
 */

class JBSApacheSolrSearchRoutines {

  private $searchParameters = FALSE;
  private $searchResults = FALSE;
  private $fieldToGetFromNode = FALSE;

  public function __construct($search_parameters = FALSE) {
    if ($search_parameters) {
      $this->searchParameters = $search_parameters;
      $this->searchResults = $this->getsSearchResults();
      $this->fieldToGetFromNode = $this->whichFieldsFromNode();
    }
  }

  private function getsSearchResults() {

    if (!$this->searchParameters) {
      return FALSE;
    }

    $key = $this->searchParameters->key;
    $conditions = array();
    $conditions['sort'] = $this->searchParameters->sort;

    if (count($this->searchParameters->fq)) {
      foreach ($this->searchParameters->fq as $fq) {
        $conditions['fq'][] = $fq;
      }
    }

    apachesolr_default_environment($this->searchParameters->environment);

    return apachesolr_search_search_execute($key, $conditions);

  }

  private function whichFieldsFromNode() {
    $fields = array();
    $result_fields = $this->searchResults[0]['fields'];
    foreach ($this->searchParameters->outputFields as $of) {
      if ((substr($of['name'],0,6)=='field_') && !array_key_exists($of['index'], $result_fields)) {
        $fields[] = $of['name'];
      }
    }
    return (count($fields)>0) ? $fields : FALSE;
  }


  public function outputTable($classes = FALSE, $caption = FALSE) {

    if (!$this->searchResults) {
      return FALSE;
    }

    $get_results_info = new stdClass();
    $get_results_info->solrResults = $this->searchResults;
    $get_results_info->outputFields = $this->searchParameters->outputFields;

    if ($this->fieldToGetFromNode) {
      $results_info = new JBSApacheSolrGetResultsInfoFromNode($get_results_info);
      $rows = $results_info->getResultsInfo();
    }
    else {
      $results_info = new JBSApacheSolrGetResultsInfoFromSolr($get_results_info);
      $rows = $results_info->getResultsInfo();
    }

    $table_rows = array();
    foreach ($rows as $row) {
      $table_row_cols = array();
      foreach ($row as $index => $col) {
        $col_defs = array (
          'class' => array('tcol', 'tcol-' . ($index + 1)),
          'data' => $col,
        );
        if ($index==0) {
          $col_defs += array('scope' => array('row'));
        }
        $table_row_cols[] = $col_defs;
      }
      $table_rows[] = $table_row_cols;
    }

    $header = array();
    foreach ($this->searchParameters->outputFields as $index => $output_info) {
      $header[] = array(
        'class' => array('tcol', 'tcol-' . ($index + 1)),
        'data' => $output_info['label'],
        'scope' => array('col'),
      );
    }

    $attributes = array('class' => array('jbs-solr-results-table'));

    $html = '';

    $table = array(
      'header' => $header,
      'rows' => $table_rows,
      'attributes' => $attributes,
      'caption' => ($caption) ? $caption : '',
      'colgroups' => NULL,
      'sticky' => FALSE,
      'empty' => FALSE,
    );

    $html .= theme_table($table);

    $html = str_replace('<td class="tcol tcol-1" scope="row">', '<th class="tcol tcol-1" scope="row">', $html);
    $html = str_replace('</td><td class="tcol tcol-2">', '</th><td class="tcol tcol-2">', $html);

    if ($classes && is_array($classes)) {
      $html = '<div class="' . implode(', ', $classes) . '">' . $html . '</div>';
    }

    pager_default_initialize($GLOBALS['pager_total_items'][0], $GLOBALS['pager_limits'][0], $GLOBALS['pager_page_array'][0]);
    $html .= '<div class="jbs-solr-results-pager">' . theme('pager') . '</div>';

    return $html;

  }

}
