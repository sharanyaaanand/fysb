/**
 * Created by rmachado on 9/23/14.
 */
function clear_form_elements(ele) {
  document.getElementById('edit-sls-title').value = '';
  document.getElementById('edit-sls-authors').value = '';
  document.getElementById('edit-sls-type').selectedIndex = -1;
  document.getElementById('edit-sls-type-keywords').value = '';
  document.getElementById('edit-sls-sel-keywords').selectedIndex = 0;
}

