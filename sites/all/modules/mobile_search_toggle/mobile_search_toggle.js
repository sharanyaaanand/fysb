(function ($) {
  Drupal.behaviors.ad_calendar = {
    attach: function (context, settings) {
     $("#toggle-search").click(function() {
     	$("#block-mobile-search-toggle-mobile-search-toggle").toggleClass('closed');
        $("#block-search-form").slideToggle('fast');
        return false;
     });

    }
  };

})(jQuery);
