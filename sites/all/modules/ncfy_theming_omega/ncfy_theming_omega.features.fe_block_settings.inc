<?php
/**
 * @file
 * ncfy_theming_omega.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function ncfy_theming_omega_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['menu-menu-social-media'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'menu-social-media',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'ncfy_omega' => array(
        'region' => 'social_media',
        'status' => 1,
        'theme' => 'ncfy_omega',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
