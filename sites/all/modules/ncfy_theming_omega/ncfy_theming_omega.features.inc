<?php
/**
 * @file
 * ncfy_theming_omega.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ncfy_theming_omega_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
