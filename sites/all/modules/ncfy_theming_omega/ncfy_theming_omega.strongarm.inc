<?php
/**
 * @file
 * ncfy_theming_omega.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function ncfy_theming_omega_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'color_ncfy_omega_files';
  $strongarm->value = '';
  $export['color_ncfy_omega_files'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'color_ncfy_omega_logo';
  $strongarm->value = '';
  $export['color_ncfy_omega_logo'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'color_ncfy_omega_palette';
  $strongarm->value = '';
  $export['color_ncfy_omega_palette'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'color_ncfy_omega_screenshot';
  $strongarm->value = '';
  $export['color_ncfy_omega_screenshot'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'color_ncfy_omega_stylesheets';
  $strongarm->value = '';
  $export['color_ncfy_omega_stylesheets'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'theme_ncfy_omega_settings';
  $strongarm->value = array(
    'toggle_logo' => 1,
    'toggle_name' => 0,
    'toggle_slogan' => 0,
    'toggle_node_user_picture' => 0,
    'toggle_comment_user_picture' => 0,
    'toggle_comment_user_verification' => '1',
    'toggle_favicon' => 1,
    'toggle_main_menu' => 1,
    'toggle_secondary_menu' => 0,
    'default_logo' => 0,
    'logo_path' => 'sites/all/themes/ncfy_omega/images/RHY_WEB_LOGO_v2.png',
    'logo_upload' => '',
    'default_favicon' => 0,
    'favicon_path' => 'public://favicon_fysb.png',
    'favicon_upload' => '',
    'alpha_grid' => 'alpha_default',
    'alpha_responsive' => 1,
    'alpha_viewport' => 1,
    'alpha_viewport_initial_scale' => '1',
    'alpha_viewport_min_scale' => '1',
    'alpha_viewport_max_scale' => '1',
    'alpha_viewport_user_scaleable' => 0,
    'alpha_primary_alpha_default' => 'normal',
    'alpha_layouts_alpha_default_fluid_responsive' => 1,
    'alpha_layouts_alpha_default_fluid_weight' => '0',
    'alpha_layouts_alpha_default_fluid_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
    'alpha_layouts_alpha_default_narrow_responsive' => 1,
    'alpha_layouts_alpha_default_narrow_weight' => '1',
    'alpha_layouts_alpha_default_narrow_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
    'alpha_layouts_alpha_default_normal_responsive' => 1,
    'alpha_layouts_alpha_default_normal_weight' => '2',
    'alpha_layouts_alpha_default_normal_media' => 'all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)',
    'alpha_layouts_alpha_default_wide_responsive' => 0,
    'alpha_layouts_alpha_default_wide_weight' => '3',
    'alpha_layouts_alpha_default_wide_media' => 'all and (min-width: 1220px)',
    'alpha_primary_alpha_fluid' => 'normal',
    'alpha_layouts_alpha_fluid_normal_responsive' => 0,
    'alpha_layouts_alpha_fluid_normal_weight' => '0',
    'alpha_layouts_alpha_fluid_normal_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
    'alpha_libraries' => array(
      'omega_formalize' => 'omega_formalize',
      'omega_mediaqueries' => 'omega_mediaqueries',
      'omega_equalheights' => 'omega_equalheights',
    ),
    'alpha_css' => array(
      'alpha-reset.css' => 'alpha-reset.css',
      'alpha-mobile.css' => 'alpha-mobile.css',
      'alpha-alpha.css' => 'alpha-alpha.css',
      'global.css' => 'global.css',
      'styles.css' => 'styles.css',
      'omega-text.css' => 0,
      'omega-branding.css' => 0,
      'omega-menu.css' => 0,
      'omega-forms.css' => 0,
      'omega-visuals.css' => 0,
    ),
    'alpha_exclude' => array(
      'sites/all/modules/adaptive_image/css/adaptive-image.css' => 0,
      'modules/book/book.css' => 0,
      'sites/all/modules/calendar/css/calendar_multiday.css' => 0,
      'sites/all/modules/date/date_api/date.css' => 0,
      'sites/all/modules/date/date_popup/themes/datepicker.1.7.css' => 0,
      'sites/all/modules/extlink/extlink.css' => 0,
      'modules/field/theme/field.css' => 0,
      'sites/all/modules/fitvids/fitvids.css' => 0,
      'modules/node/node.css' => 0,
      'modules/search/search.css' => 0,
      'sites/all/modules/custom/solr_library_search/css/solr_library_search.css' => 0,
      'sites/all/modules/custom/solr_menu_alter/css/solr_menu_alter.css' => 0,
      'modules/user/user.css' => 0,
      'sites/all/modules/views/css/views.css' => 0,
      'sites/all/modules/youtube/css/youtube.css' => 0,
      'misc/vertical-tabs.css' => 0,
      'modules/aggregator/aggregator.css' => 0,
      'modules/block/block.css' => 0,
      'modules/dblog/dblog.css' => 0,
      'modules/file/file.css' => 0,
      'modules/filter/filter.css' => 0,
      'modules/help/help.css' => 0,
      'modules/menu/menu.css' => 0,
      'modules/openid/openid.css' => 0,
      'modules/profile/profile.css' => 0,
      'modules/statistics/statistics.css' => 0,
      'modules/syslog/syslog.css' => 0,
      'modules/system/admin.css' => 0,
      'modules/system/maintenance.css' => 0,
      'modules/system/system.css' => 0,
      'modules/system/system.admin.css' => 0,
      'modules/system/system.base.css' => 0,
      'modules/system/system.maintenance.css' => 0,
      'modules/system/system.menus.css' => 0,
      'modules/system/system.messages.css' => 0,
      'modules/system/system.theme.css' => 0,
      'modules/taxonomy/taxonomy.css' => 0,
      'modules/tracker/tracker.css' => 0,
      'modules/update/update.css' => 0,
    ),
    'alpha_debug_block_toggle' => 0,
    'alpha_debug_block_active' => 1,
    'alpha_debug_grid_toggle' => 0,
    'alpha_debug_grid_active' => 1,
    'alpha_debug_grid_roles' => array(
      3 => '3',
      1 => 0,
      2 => 0,
      5 => 0,
      6 => 0,
      4 => 0,
      7 => 0,
      8 => 0,
    ),
    'alpha_toggle_messages' => 1,
    'alpha_toggle_action_links' => 1,
    'alpha_toggle_tabs' => 1,
    'alpha_toggle_breadcrumb' => 0,
    'alpha_toggle_page_title' => 1,
    'alpha_toggle_feed_icons' => 1,
    'alpha_hidden_title' => 0,
    'alpha_hidden_site_name' => 0,
    'alpha_hidden_site_slogan' => 0,
    'alpha_region_stage_menu_force' => 0,
    'alpha_region_stage_menu_zone' => NULL,
    'alpha_region_stage_menu_prefix' => '0',
    'alpha_region_stage_menu_columns' => '1',
    'alpha_region_stage_menu_suffix' => '0',
    'alpha_region_stage_menu_weight' => '0',
    'alpha_region_stage_menu_position' => '0',
    'alpha_region_stage_menu_css' => '',
    'alpha_region_stage_menu_equal_height_element' => FALSE,
    'alpha_region_stage_menu_equal_height_container' => 0,
    'alpha_zone_second_menu_wrapper' => 0,
    'alpha_zone_second_menu_force' => 0,
    'alpha_zone_second_menu_order' => 0,
    'alpha_zone_second_menu_section' => NULL,
    'alpha_zone_second_menu_weight' => '0',
    'alpha_zone_second_menu_columns' => '12',
    'alpha_zone_second_menu_primary' => NULL,
    'alpha_zone_second_menu_css' => '',
    'alpha_zone_second_menu_wrapper_css' => '',
    'alpha_zone_second_menu_equal_height_container' => 0,
    'alpha_zone_user_wrapper' => 1,
    'alpha_zone_user_force' => 0,
    'alpha_zone_user_order' => 0,
    'alpha_zone_user_section' => 'header',
    'alpha_zone_user_weight' => '1',
    'alpha_zone_user_columns' => '12',
    'alpha_zone_user_primary' => NULL,
    'alpha_zone_user_css' => '',
    'alpha_zone_user_wrapper_css' => '',
    'alpha_zone_user_equal_height_container' => 0,
    'alpha_region_user_first_force' => 0,
    'alpha_region_user_first_zone' => 'user',
    'alpha_region_user_first_prefix' => '0',
    'alpha_region_user_first_columns' => '9',
    'alpha_region_user_first_suffix' => '0',
    'alpha_region_user_first_weight' => '1',
    'alpha_region_user_first_position' => '0',
    'alpha_region_user_first_css' => 'alpha',
    'alpha_region_user_first_equal_height_element' => 0,
    'alpha_region_user_first_equal_height_container' => 0,
    'alpha_region_user_second_force' => 0,
    'alpha_region_user_second_zone' => 'user',
    'alpha_region_user_second_prefix' => '9',
    'alpha_region_user_second_columns' => '3',
    'alpha_region_user_second_suffix' => '0',
    'alpha_region_user_second_weight' => '2',
    'alpha_region_user_second_position' => '0',
    'alpha_region_user_second_css' => 'omega',
    'alpha_region_user_second_equal_height_element' => 0,
    'alpha_region_user_second_equal_height_container' => 0,
    'alpha_zone_branding_wrapper' => 1,
    'alpha_zone_branding_force' => 0,
    'alpha_zone_branding_order' => 0,
    'alpha_zone_branding_section' => 'header',
    'alpha_zone_branding_weight' => '2',
    'alpha_zone_branding_columns' => '12',
    'alpha_zone_branding_primary' => NULL,
    'alpha_zone_branding_css' => '',
    'alpha_zone_branding_wrapper_css' => '',
    'alpha_zone_branding_equal_height_container' => 0,
    'alpha_region_branding_force' => 1,
    'alpha_region_branding_zone' => 'branding',
    'alpha_region_branding_prefix' => '0',
    'alpha_region_branding_columns' => '8',
    'alpha_region_branding_suffix' => '0',
    'alpha_region_branding_weight' => '0',
    'alpha_region_branding_position' => '0',
    'alpha_region_branding_css' => 'alpha',
    'alpha_region_branding_equal_height_element' => 0,
    'alpha_region_branding_equal_height_container' => 0,
    'alpha_region_search_bar_force' => 1,
    'alpha_region_search_bar_zone' => 'branding',
    'alpha_region_search_bar_prefix' => '0',
    'alpha_region_search_bar_columns' => '4',
    'alpha_region_search_bar_suffix' => '0',
    'alpha_region_search_bar_weight' => '1',
    'alpha_region_search_bar_position' => '0',
    'alpha_region_search_bar_css' => 'omega',
    'alpha_region_search_bar_equal_height_element' => 0,
    'alpha_region_search_bar_equal_height_container' => 0,
    'alpha_region_social_media_force' => 1,
    'alpha_region_social_media_zone' => 'branding',
    'alpha_region_social_media_prefix' => '0',
    'alpha_region_social_media_columns' => '3',
    'alpha_region_social_media_suffix' => '0',
    'alpha_region_social_media_weight' => '2',
    'alpha_region_social_media_position' => '0',
    'alpha_region_social_media_css' => 'omega',
    'alpha_region_social_media_equal_height_element' => 0,
    'alpha_region_social_media_equal_height_container' => 0,
    'alpha_region_chat_block_force' => 1,
    'alpha_region_chat_block_zone' => 'branding',
    'alpha_region_chat_block_prefix' => '0',
    'alpha_region_chat_block_columns' => '3',
    'alpha_region_chat_block_suffix' => '0',
    'alpha_region_chat_block_weight' => '3',
    'alpha_region_chat_block_position' => '0',
    'alpha_region_chat_block_css' => 'omega',
    'alpha_region_chat_block_equal_height_element' => 0,
    'alpha_region_chat_block_equal_height_container' => 0,
    'alpha_zone_menu_wrapper' => 1,
    'alpha_zone_menu_force' => 0,
    'alpha_zone_menu_order' => 0,
    'alpha_zone_menu_section' => 'header',
    'alpha_zone_menu_weight' => '3',
    'alpha_zone_menu_columns' => '12',
    'alpha_zone_menu_primary' => NULL,
    'alpha_zone_menu_css' => '',
    'alpha_zone_menu_wrapper_css' => '',
    'alpha_zone_menu_equal_height_container' => 0,
    'alpha_region_menu_force' => 1,
    'alpha_region_menu_zone' => 'menu',
    'alpha_region_menu_prefix' => '0',
    'alpha_region_menu_columns' => '12',
    'alpha_region_menu_suffix' => '0',
    'alpha_region_menu_weight' => '1',
    'alpha_region_menu_position' => '0',
    'alpha_region_menu_css' => '',
    'alpha_region_menu_equal_height_element' => 0,
    'alpha_region_menu_equal_height_container' => 0,
    'alpha_zone_preface_wrapper' => 1,
    'alpha_zone_preface_force' => 0,
    'alpha_zone_preface_order' => 0,
    'alpha_zone_preface_section' => 'content',
    'alpha_zone_preface_weight' => '1',
    'alpha_zone_preface_columns' => '12',
    'alpha_zone_preface_primary' => NULL,
    'alpha_zone_preface_css' => '',
    'alpha_zone_preface_wrapper_css' => '',
    'alpha_zone_preface_equal_height_container' => 1,
    'alpha_region_preface_first_force' => 0,
    'alpha_region_preface_first_zone' => 'preface',
    'alpha_region_preface_first_prefix' => '0',
    'alpha_region_preface_first_columns' => '8',
    'alpha_region_preface_first_suffix' => '0',
    'alpha_region_preface_first_weight' => '1',
    'alpha_region_preface_first_position' => '0',
    'alpha_region_preface_first_css' => 'alpha',
    'alpha_region_preface_first_equal_height_element' => 1,
    'alpha_region_preface_first_equal_height_container' => 0,
    'alpha_region_preface_second_force' => 0,
    'alpha_region_preface_second_zone' => 'preface',
    'alpha_region_preface_second_prefix' => '0',
    'alpha_region_preface_second_columns' => '4',
    'alpha_region_preface_second_suffix' => '0',
    'alpha_region_preface_second_weight' => '2',
    'alpha_region_preface_second_position' => '0',
    'alpha_region_preface_second_css' => 'omega',
    'alpha_region_preface_second_equal_height_element' => 1,
    'alpha_region_preface_second_equal_height_container' => 0,
    'alpha_zone_content_wrapper' => 1,
    'alpha_zone_content_force' => 1,
    'alpha_zone_content_order' => 0,
    'alpha_zone_content_section' => 'content',
    'alpha_zone_content_weight' => '2',
    'alpha_zone_content_columns' => '12',
    'alpha_zone_content_primary' => 'content',
    'alpha_zone_content_css' => '',
    'alpha_zone_content_wrapper_css' => '',
    'alpha_zone_content_equal_height_container' => 0,
    'alpha_region_content_force' => 0,
    'alpha_region_content_zone' => 'content',
    'alpha_region_content_prefix' => '0',
    'alpha_region_content_columns' => '8',
    'alpha_region_content_suffix' => '0',
    'alpha_region_content_weight' => '1',
    'alpha_region_content_position' => '0',
    'alpha_region_content_css' => 'alpha feature-left',
    'alpha_region_content_equal_height_element' => 0,
    'alpha_region_content_equal_height_container' => 0,
    'alpha_region_sidebar_first_force' => 0,
    'alpha_region_sidebar_first_zone' => 'content',
    'alpha_region_sidebar_first_prefix' => '0',
    'alpha_region_sidebar_first_columns' => '4',
    'alpha_region_sidebar_first_suffix' => '0',
    'alpha_region_sidebar_first_weight' => '2',
    'alpha_region_sidebar_first_position' => '0',
    'alpha_region_sidebar_first_css' => 'omega feature-right',
    'alpha_region_sidebar_first_equal_height_element' => 0,
    'alpha_region_sidebar_first_equal_height_container' => 0,
    'alpha_zone_postscript_wrapper' => 1,
    'alpha_zone_postscript_force' => 0,
    'alpha_zone_postscript_order' => 0,
    'alpha_zone_postscript_section' => 'content',
    'alpha_zone_postscript_weight' => '3',
    'alpha_zone_postscript_columns' => '12',
    'alpha_zone_postscript_primary' => 'postscript_half_1',
    'alpha_zone_postscript_css' => '',
    'alpha_zone_postscript_wrapper_css' => '',
    'alpha_zone_postscript_equal_height_container' => 0,
    'alpha_region_postscript_half_1_force' => 0,
    'alpha_region_postscript_half_1_zone' => 'postscript',
    'alpha_region_postscript_half_1_prefix' => '0',
    'alpha_region_postscript_half_1_columns' => '8',
    'alpha_region_postscript_half_1_suffix' => '0',
    'alpha_region_postscript_half_1_weight' => '1',
    'alpha_region_postscript_half_1_position' => '0',
    'alpha_region_postscript_half_1_css' => 'alpha',
    'alpha_region_postscript_half_1_equal_height_element' => 0,
    'alpha_region_postscript_half_1_equal_height_container' => 0,
    'alpha_region_postscript_half_2_force' => 0,
    'alpha_region_postscript_half_2_zone' => 'postscript',
    'alpha_region_postscript_half_2_prefix' => '0',
    'alpha_region_postscript_half_2_columns' => '4',
    'alpha_region_postscript_half_2_suffix' => '0',
    'alpha_region_postscript_half_2_weight' => '2',
    'alpha_region_postscript_half_2_position' => '0',
    'alpha_region_postscript_half_2_css' => 'omega',
    'alpha_region_postscript_half_2_equal_height_element' => 0,
    'alpha_region_postscript_half_2_equal_height_container' => 0,
    'alpha_zone_postscript_second_wrapper' => 0,
    'alpha_zone_postscript_second_force' => 0,
    'alpha_zone_postscript_second_order' => 0,
    'alpha_zone_postscript_second_section' => 'content',
    'alpha_zone_postscript_second_weight' => '4',
    'alpha_zone_postscript_second_columns' => '12',
    'alpha_zone_postscript_second_primary' => NULL,
    'alpha_zone_postscript_second_css' => '',
    'alpha_zone_postscript_second_wrapper_css' => '',
    'alpha_zone_postscript_second_equal_height_container' => 0,
    'alpha_region_postcript_third_1_force' => 0,
    'alpha_region_postcript_third_1_zone' => 'postscript_second',
    'alpha_region_postcript_third_1_prefix' => '0',
    'alpha_region_postcript_third_1_columns' => '4',
    'alpha_region_postcript_third_1_suffix' => '0',
    'alpha_region_postcript_third_1_weight' => '1',
    'alpha_region_postcript_third_1_position' => '0',
    'alpha_region_postcript_third_1_css' => 'alpha',
    'alpha_region_postcript_third_1_equal_height_element' => 0,
    'alpha_region_postcript_third_1_equal_height_container' => 0,
    'alpha_region_postcript_third_2_force' => 0,
    'alpha_region_postcript_third_2_zone' => 'postscript_second',
    'alpha_region_postcript_third_2_prefix' => '0',
    'alpha_region_postcript_third_2_columns' => '4',
    'alpha_region_postcript_third_2_suffix' => '0',
    'alpha_region_postcript_third_2_weight' => '2',
    'alpha_region_postcript_third_2_position' => '0',
    'alpha_region_postcript_third_2_css' => 'alpha',
    'alpha_region_postcript_third_2_equal_height_element' => 0,
    'alpha_region_postcript_third_2_equal_height_container' => 0,
    'alpha_region_postcript_third_3_force' => 0,
    'alpha_region_postcript_third_3_zone' => 'postscript_second',
    'alpha_region_postcript_third_3_prefix' => '0',
    'alpha_region_postcript_third_3_columns' => '4',
    'alpha_region_postcript_third_3_suffix' => '0',
    'alpha_region_postcript_third_3_weight' => '3',
    'alpha_region_postcript_third_3_position' => '0',
    'alpha_region_postcript_third_3_css' => 'omega',
    'alpha_region_postcript_third_3_equal_height_element' => 0,
    'alpha_region_postcript_third_3_equal_height_container' => 0,
    'alpha_zone_footer_wrapper' => 1,
    'alpha_zone_footer_force' => 0,
    'alpha_zone_footer_order' => 0,
    'alpha_zone_footer_section' => 'footer',
    'alpha_zone_footer_weight' => '1',
    'alpha_zone_footer_columns' => '12',
    'alpha_zone_footer_primary' => NULL,
    'alpha_zone_footer_css' => '',
    'alpha_zone_footer_wrapper_css' => '',
    'alpha_zone_footer_equal_height_container' => 0,
    'alpha_region_footer_first_force' => 0,
    'alpha_region_footer_first_zone' => 'footer',
    'alpha_region_footer_first_prefix' => '0',
    'alpha_region_footer_first_columns' => '8',
    'alpha_region_footer_first_suffix' => '0',
    'alpha_region_footer_first_weight' => '1',
    'alpha_region_footer_first_position' => '0',
    'alpha_region_footer_first_css' => 'alpha',
    'alpha_region_footer_first_equal_height_element' => 0,
    'alpha_region_footer_first_equal_height_container' => 0,
    'alpha_region_footer_second_force' => 0,
    'alpha_region_footer_second_zone' => 'footer',
    'alpha_region_footer_second_prefix' => '0',
    'alpha_region_footer_second_columns' => '4',
    'alpha_region_footer_second_suffix' => '0',
    'alpha_region_footer_second_weight' => '2',
    'alpha_region_footer_second_position' => '0',
    'alpha_region_footer_second_css' => 'omega',
    'alpha_region_footer_second_equal_height_element' => 0,
    'alpha_region_footer_second_equal_height_container' => 0,
    'responsive_tables_medium_priority_max_width' => '980px',
    'responsive_tables_low_priority_max_width' => '740px',
    'favicon_mimetype' => 'image/png',
  );
  $export['theme_ncfy_omega_settings'] = $strongarm;

  return $export;
}
