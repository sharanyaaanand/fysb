<?php
/**
 * @file
 * ncfy_menus.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function ncfy_menus_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-social-media.
  $menus['menu-social-media'] = array(
    'menu_name' => 'menu-social-media',
    'title' => 'Social Media Menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Social Media Menu');

  return $menus;
}
