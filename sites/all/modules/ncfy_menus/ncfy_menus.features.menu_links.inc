<?php
/**
 * @file
 * ncfy_menus.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function ncfy_menus_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-social-media_facebook:https://www.facebook.com/NCFYgov/.
  $menu_links['menu-social-media_facebook:https://www.facebook.com/NCFYgov/'] = array(
    'menu_name' => 'menu-social-media',
    'link_path' => 'https://www.facebook.com/NCFYgov/',
    'router_path' => '',
    'link_title' => 'Facebook',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'menu-social-media_facebook:https://www.facebook.com/NCFYgov/',
      'attributes' => array(
        'class' => array(
          0 => 'menu-link--facebook',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => 'menu-item--facebook',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-social-media_twitter:https://twitter.com/ncfy.
  $menu_links['menu-social-media_twitter:https://twitter.com/ncfy'] = array(
    'menu_name' => 'menu-social-media',
    'link_path' => 'https://twitter.com/ncfy',
    'router_path' => '',
    'link_title' => 'Twitter',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'menu-social-media_twitter:https://twitter.com/ncfy',
      'attributes' => array(
        'class' => array(
          0 => 'menu-link--twitter',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => 'menu-item--twitter',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Facebook');
  t('Twitter');

  return $menu_links;
}
