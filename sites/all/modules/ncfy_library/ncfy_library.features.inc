<?php
/**
 * @file
 * ncfy_library.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ncfy_library_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ncfy_library_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function ncfy_library_node_info() {
  $items = array(
    'library' => array(
      'name' => t('Library'),
      'base' => 'node_content',
      'description' => t('Use this to add records to the NCFY Online Literature Database.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
