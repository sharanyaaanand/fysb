<?php
/**
 * @file
 * ncfy_library.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function ncfy_library_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:library
  $menu_links['main-menu:library'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'library',
    'router_path' => 'library',
    'link_title' => 'Library',
    'options' => array(
      'attributes' => array(
        'title' => 'Search the literature database',
      ),
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 20,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Library');


  return $menu_links;
}
