<?php
/**
 * @file
 * ncfy_library.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ncfy_library_user_default_permissions() {
  $permissions = array();

  // Exported permission: create library content.
  $permissions['create library content'] = array(
    'name' => 'create library content',
    'roles' => array(
      'Content Manager' => 'Content Manager',
      'JBS admin' => 'JBS admin',
      'SysOp' => 'SysOp',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any library content.
  $permissions['delete any library content'] = array(
    'name' => 'delete any library content',
    'roles' => array(
      'Content Manager' => 'Content Manager',
      'JBS admin' => 'JBS admin',
      'SysOp' => 'SysOp',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own library content.
  $permissions['delete own library content'] = array(
    'name' => 'delete own library content',
    'roles' => array(
      'Content Manager' => 'Content Manager',
      'JBS admin' => 'JBS admin',
      'SysOp' => 'SysOp',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any library content.
  $permissions['edit any library content'] = array(
    'name' => 'edit any library content',
    'roles' => array(
      'Content Manager' => 'Content Manager',
      'JBS admin' => 'JBS admin',
      'SysOp' => 'SysOp',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own library content.
  $permissions['edit own library content'] = array(
    'name' => 'edit own library content',
    'roles' => array(
      'Content Manager' => 'Content Manager',
      'JBS admin' => 'JBS admin',
      'SysOp' => 'SysOp',
    ),
    'module' => 'node',
  );

  return $permissions;
}
