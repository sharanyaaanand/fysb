<?php
/**
 * @file
 * ncfy_library.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ncfy_library_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'library_search';
  $view->description = 'Search the NCFY Literature Database';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Library Search';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Library Search';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Clear';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_library_year_published' => 'field_library_year_published',
    'title' => 'title',
    'field_library_authors' => 'field_library_authors',
    'field_library_description' => 'field_library_description',
    'field_document' => 'field_document',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'field_library_year_published' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_library_authors' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_library_description' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_document' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['caption'] = 'Library search results';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['label'] = 'Search page blurb';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<p><img src="sites/default/files/library_search.jpg" alt="library search" width="330" height="165" vspace="10" hspace="5" align="right" />The NCFY Library is a literature database containing abstracts, or summaries, of more than 20,000 publications that focus on youth and family issues.</p>
<p>Each entry includes information on obtaining the publication. When possible, we provide direct links to PDF files of open-access publications. </p>
<p>Publications in the database do not necessarily reflect the views of NCFY, the Family and Youth Services Bureau or the Administration for Children and Families.</p>
<p>&nbsp;</p>';
  $handler->display->display_options['header']['area']['format'] = '2';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'No Results';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'Your search found no records.';
  $handler->display->display_options['empty']['area']['format'] = '7';
  /* Field: Content: Year Published */
  $handler->display->display_options['fields']['field_library_year_published']['id'] = 'field_library_year_published';
  $handler->display->display_options['fields']['field_library_year_published']['table'] = 'field_data_field_library_year_published';
  $handler->display->display_options['fields']['field_library_year_published']['field'] = 'field_library_year_published';
  $handler->display->display_options['fields']['field_library_year_published']['label'] = 'Year';
  $handler->display->display_options['fields']['field_library_year_published']['settings'] = array(
    'format_type' => 'year',
    'fromto' => '',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Authors */
  $handler->display->display_options['fields']['field_library_authors']['id'] = 'field_library_authors';
  $handler->display->display_options['fields']['field_library_authors']['table'] = 'field_data_field_library_authors';
  $handler->display->display_options['fields']['field_library_authors']['field'] = 'field_library_authors';
  $handler->display->display_options['fields']['field_library_authors']['settings'] = array(
    'field_delimiter' => '',
  );
  $handler->display->display_options['fields']['field_library_authors']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_library_authors']['separator'] = '';
  /* Field: Content: Type */
  $handler->display->display_options['fields']['field_library_description']['id'] = 'field_library_description';
  $handler->display->display_options['fields']['field_library_description']['table'] = 'field_data_field_library_description';
  $handler->display->display_options['fields']['field_library_description']['field'] = 'field_library_description';
  /* Field: Content: Link to Document */
  $handler->display->display_options['fields']['field_library_document_link']['id'] = 'field_library_document_link';
  $handler->display->display_options['fields']['field_library_document_link']['table'] = 'field_data_field_library_document_link';
  $handler->display->display_options['fields']['field_library_document_link']['field'] = 'field_library_document_link';
  $handler->display->display_options['fields']['field_library_document_link']['click_sort_column'] = 'url';
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Year Published (field_library_year_published) */
  $handler->display->display_options['sorts']['field_library_year_published_value']['id'] = 'field_library_year_published_value';
  $handler->display->display_options['sorts']['field_library_year_published_value']['table'] = 'field_data_field_library_year_published';
  $handler->display->display_options['sorts']['field_library_year_published_value']['field'] = 'field_library_year_published_value';
  $handler->display->display_options['sorts']['field_library_year_published_value']['order'] = 'DESC';
  $handler->display->display_options['sorts']['field_library_year_published_value']['expose']['label'] = 'Year Published (field_library_year_published)';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'library' => 'library',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Global: Combine fields filter */
  $handler->display->display_options['filters']['combine']['id'] = 'combine';
  $handler->display->display_options['filters']['combine']['table'] = 'views';
  $handler->display->display_options['filters']['combine']['field'] = 'combine';
  $handler->display->display_options['filters']['combine']['operator'] = 'contains';
  $handler->display->display_options['filters']['combine']['group'] = 1;
  $handler->display->display_options['filters']['combine']['exposed'] = TRUE;
  $handler->display->display_options['filters']['combine']['expose']['operator_id'] = 'combine_op';
  $handler->display->display_options['filters']['combine']['expose']['label'] = 'Combine fields filter';
  $handler->display->display_options['filters']['combine']['expose']['operator'] = 'combine_op';
  $handler->display->display_options['filters']['combine']['expose']['identifier'] = 'combine';
  $handler->display->display_options['filters']['combine']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    5 => 0,
    6 => 0,
    4 => 0,
    3 => 0,
    7 => 0,
  );
  $handler->display->display_options['filters']['combine']['fields'] = array(
    'title' => 'title',
    'body' => 'body',
  );
  /* Filter criterion: Content: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['operator'] = 'word';
  $handler->display->display_options['filters']['title']['group'] = 1;
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    3 => 0,
    7 => 0,
  );
  /* Filter criterion: Content: Authors (field_library_authors) */
  $handler->display->display_options['filters']['field_library_authors_value']['id'] = 'field_library_authors_value';
  $handler->display->display_options['filters']['field_library_authors_value']['table'] = 'field_data_field_library_authors';
  $handler->display->display_options['filters']['field_library_authors_value']['field'] = 'field_library_authors_value';
  $handler->display->display_options['filters']['field_library_authors_value']['operator'] = 'word';
  $handler->display->display_options['filters']['field_library_authors_value']['group'] = 1;
  $handler->display->display_options['filters']['field_library_authors_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_library_authors_value']['expose']['operator_id'] = 'field_library_authors_value_op';
  $handler->display->display_options['filters']['field_library_authors_value']['expose']['label'] = 'Authors';
  $handler->display->display_options['filters']['field_library_authors_value']['expose']['operator'] = 'field_library_authors_value_op';
  $handler->display->display_options['filters']['field_library_authors_value']['expose']['identifier'] = 'field_library_authors_value';
  $handler->display->display_options['filters']['field_library_authors_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    3 => 0,
    7 => 0,
  );
  /* Filter criterion: Content: Type (field_library_description) */
  $handler->display->display_options['filters']['field_library_description_value']['id'] = 'field_library_description_value';
  $handler->display->display_options['filters']['field_library_description_value']['table'] = 'field_data_field_library_description';
  $handler->display->display_options['filters']['field_library_description_value']['field'] = 'field_library_description_value';
  $handler->display->display_options['filters']['field_library_description_value']['group'] = 1;
  $handler->display->display_options['filters']['field_library_description_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_library_description_value']['expose']['operator_id'] = 'field_library_description_value_op';
  $handler->display->display_options['filters']['field_library_description_value']['expose']['label'] = 'Type';
  $handler->display->display_options['filters']['field_library_description_value']['expose']['operator'] = 'field_library_description_value_op';
  $handler->display->display_options['filters']['field_library_description_value']['expose']['identifier'] = 'field_library_description_value';
  $handler->display->display_options['filters']['field_library_description_value']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_library_description_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    3 => 0,
    7 => 0,
  );
  $handler->display->display_options['filters']['field_library_description_value']['reduce_duplicates'] = TRUE;
  /* Filter criterion: Content: Year Published (field_library_year_published) */
  $handler->display->display_options['filters']['field_library_year_published_value']['id'] = 'field_library_year_published_value';
  $handler->display->display_options['filters']['field_library_year_published_value']['table'] = 'field_data_field_library_year_published';
  $handler->display->display_options['filters']['field_library_year_published_value']['field'] = 'field_library_year_published_value';
  $handler->display->display_options['filters']['field_library_year_published_value']['group'] = 1;
  $handler->display->display_options['filters']['field_library_year_published_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_library_year_published_value']['expose']['operator_id'] = 'field_library_year_published_value_op';
  $handler->display->display_options['filters']['field_library_year_published_value']['expose']['label'] = 'Year Published';
  $handler->display->display_options['filters']['field_library_year_published_value']['expose']['operator'] = 'field_library_year_published_value_op';
  $handler->display->display_options['filters']['field_library_year_published_value']['expose']['identifier'] = 'field_library_year_published_value';
  $handler->display->display_options['filters']['field_library_year_published_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    3 => 0,
    7 => 0,
  );
  $handler->display->display_options['filters']['field_library_year_published_value']['granularity'] = 'year';
  $handler->display->display_options['filters']['field_library_year_published_value']['year_range'] = '-40:+0';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '3';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_library_year_published' => 'field_library_year_published',
    'title' => 'title',
    'field_library_authors' => 'field_library_authors',
    'field_library_description' => 'field_library_description',
    'field_library_document_link' => 'field_library_document_link',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'field_library_year_published' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_library_authors' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_library_description' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_library_document_link' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['caption'] = 'Library Search Results';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'library' => 'library',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Global: Combine fields filter */
  $handler->display->display_options['filters']['combine']['id'] = 'combine';
  $handler->display->display_options['filters']['combine']['table'] = 'views';
  $handler->display->display_options['filters']['combine']['field'] = 'combine';
  $handler->display->display_options['filters']['combine']['operator'] = 'contains';
  $handler->display->display_options['filters']['combine']['group'] = 1;
  $handler->display->display_options['filters']['combine']['exposed'] = TRUE;
  $handler->display->display_options['filters']['combine']['expose']['operator_id'] = 'combine_op';
  $handler->display->display_options['filters']['combine']['expose']['label'] = 'Title and Abstract';
  $handler->display->display_options['filters']['combine']['expose']['operator'] = 'combine_op';
  $handler->display->display_options['filters']['combine']['expose']['identifier'] = 'combine';
  $handler->display->display_options['filters']['combine']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    5 => 0,
    6 => 0,
    4 => 0,
    3 => 0,
    7 => 0,
  );
  $handler->display->display_options['filters']['combine']['fields'] = array(
    'title' => 'title',
    'body' => 'body',
  );
  /* Filter criterion: Content: Authors (field_library_authors) */
  $handler->display->display_options['filters']['field_library_authors_value']['id'] = 'field_library_authors_value';
  $handler->display->display_options['filters']['field_library_authors_value']['table'] = 'field_data_field_library_authors';
  $handler->display->display_options['filters']['field_library_authors_value']['field'] = 'field_library_authors_value';
  $handler->display->display_options['filters']['field_library_authors_value']['operator'] = 'word';
  $handler->display->display_options['filters']['field_library_authors_value']['group'] = 1;
  $handler->display->display_options['filters']['field_library_authors_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_library_authors_value']['expose']['operator_id'] = 'field_library_authors_value_op';
  $handler->display->display_options['filters']['field_library_authors_value']['expose']['label'] = 'Authors';
  $handler->display->display_options['filters']['field_library_authors_value']['expose']['operator'] = 'field_library_authors_value_op';
  $handler->display->display_options['filters']['field_library_authors_value']['expose']['identifier'] = 'field_library_authors_value';
  $handler->display->display_options['filters']['field_library_authors_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    3 => 0,
    7 => 0,
  );
  /* Filter criterion: Content: Type (field_library_description) */
  $handler->display->display_options['filters']['field_library_description_value']['id'] = 'field_library_description_value';
  $handler->display->display_options['filters']['field_library_description_value']['table'] = 'field_data_field_library_description';
  $handler->display->display_options['filters']['field_library_description_value']['field'] = 'field_library_description_value';
  $handler->display->display_options['filters']['field_library_description_value']['group'] = 1;
  $handler->display->display_options['filters']['field_library_description_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_library_description_value']['expose']['operator_id'] = 'field_library_description_value_op';
  $handler->display->display_options['filters']['field_library_description_value']['expose']['label'] = 'Type';
  $handler->display->display_options['filters']['field_library_description_value']['expose']['operator'] = 'field_library_description_value_op';
  $handler->display->display_options['filters']['field_library_description_value']['expose']['identifier'] = 'field_library_description_value';
  $handler->display->display_options['filters']['field_library_description_value']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_library_description_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    3 => 0,
    7 => 0,
  );
  $handler->display->display_options['filters']['field_library_description_value']['reduce_duplicates'] = TRUE;
  /* Filter criterion: Content: Year Published (field_library_year_published) */
  $handler->display->display_options['filters']['field_library_year_published_value']['id'] = 'field_library_year_published_value';
  $handler->display->display_options['filters']['field_library_year_published_value']['table'] = 'field_data_field_library_year_published';
  $handler->display->display_options['filters']['field_library_year_published_value']['field'] = 'field_library_year_published_value';
  $handler->display->display_options['filters']['field_library_year_published_value']['group'] = 1;
  $handler->display->display_options['filters']['field_library_year_published_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_library_year_published_value']['expose']['operator_id'] = 'field_library_year_published_value_op';
  $handler->display->display_options['filters']['field_library_year_published_value']['expose']['label'] = 'Year Published';
  $handler->display->display_options['filters']['field_library_year_published_value']['expose']['operator'] = 'field_library_year_published_value_op';
  $handler->display->display_options['filters']['field_library_year_published_value']['expose']['identifier'] = 'field_library_year_published_value';
  $handler->display->display_options['filters']['field_library_year_published_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    3 => 0,
    7 => 0,
  );
  $handler->display->display_options['filters']['field_library_year_published_value']['granularity'] = 'year';
  $handler->display->display_options['filters']['field_library_year_published_value']['year_range'] = '-40:+0';
  $handler->display->display_options['path'] = 'library';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Library';
  $handler->display->display_options['menu']['description'] = 'Search the literature database';
  $handler->display->display_options['menu']['weight'] = '20';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['library_search'] = $view;

  return $export;
}
