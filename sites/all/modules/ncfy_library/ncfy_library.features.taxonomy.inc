<?php
/**
 * @file
 * ncfy_library.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ncfy_library_taxonomy_default_vocabularies() {
  return array(
    'youth_database_taxonomy' => array(
      'name' => 'Youth Database Taxonomy',
      'machine_name' => 'youth_database_taxonomy',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -5,
    ),
  );
}
