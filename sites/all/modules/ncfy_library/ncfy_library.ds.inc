<?php
/**
 * @file
 * ncfy_library.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function ncfy_library_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|library|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'library';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_library_authors',
        1 => 'field_library_year_published',
        2 => 'field_library_description',
        3 => 'field_library_language',
        4 => 'field_library_organization',
        5 => 'field_library_series',
        6 => 'field_library_source',
        7 => 'body',
        8 => 'field_library_document_link',
        9 => 'field_library_availability',
        10 => 'field_library_notes',
      ),
    ),
    'fields' => array(
      'field_library_authors' => 'ds_content',
      'field_library_year_published' => 'ds_content',
      'field_library_description' => 'ds_content',
      'field_library_language' => 'ds_content',
      'field_library_organization' => 'ds_content',
      'field_library_series' => 'ds_content',
      'field_library_source' => 'ds_content',
      'body' => 'ds_content',
      'field_library_document_link' => 'ds_content',
      'field_library_availability' => 'ds_content',
      'field_library_notes' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|library|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|library|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'library';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'title',
        1 => 'field_library_description',
        2 => 'field_library_authors',
        3 => 'field_library_year_published',
        4 => 'field_library_language',
        5 => 'field_library_organization',
        6 => 'field_library_series',
        7 => 'field_library_source',
        8 => 'body',
        9 => 'field_library_accession_number',
        10 => 'field_library_date_verified',
        11 => 'redirect',
        12 => 'path',
      ),
      'right' => array(
        13 => 'field_library_notes',
        14 => 'field_library_availability',
        15 => 'field_document',
        16 => 'field_library_availability_link',
        17 => 'field_library_keywords',
        18 => 'metatags',
        19 => 'field_library_document_link',
      ),
      'hidden' => array(
        20 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'title' => 'left',
      'field_library_description' => 'left',
      'field_library_authors' => 'left',
      'field_library_year_published' => 'left',
      'field_library_language' => 'left',
      'field_library_organization' => 'left',
      'field_library_series' => 'left',
      'field_library_source' => 'left',
      'body' => 'left',
      'field_library_accession_number' => 'left',
      'field_library_date_verified' => 'left',
      'redirect' => 'left',
      'path' => 'left',
      'field_library_notes' => 'right',
      'field_library_availability' => 'right',
      'field_document' => 'right',
      'field_library_availability_link' => 'right',
      'field_library_keywords' => 'right',
      'metatags' => 'right',
      'field_library_document_link' => 'right',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|library|form'] = $ds_layout;

  return $export;
}
