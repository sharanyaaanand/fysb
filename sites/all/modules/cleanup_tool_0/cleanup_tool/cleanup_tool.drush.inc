<?php


/**
 * @file
 * Drush implementation for the cleanup_tool module.
 */

/**
 * Implementats hook_drush_command().
 */
function cleanup_tool_drush_command() {
  $items = array();
  $items['cleanup_tool-clean'] = array(
    'description' => 'Clean orphaned modules, these modules exist in the system table but are no longer on the filesystem: Experts only, use at your own risk',
    'aliases' => array('cu-clean'),
    'arguments' => array(
      'module_name' => 'A unique name that identifies which module to remove/clean out of system table.',
    ),
    'options' => array(
      'update_option' => "This is a required option, either: --update_option=all OR --update_option=simple",
    ),
    'examples' => array(
      'drush cu-clean cleanup_bean_plugins --update_option=simple' => 'remove orphaned module called cleanup_bean_plugins from the system table.',
      'drush cu-clean all --update_option=all' => 'remove all orphaned modules found in the system table that are missing from the filesystem.',
    ),
    'drupal dependencies' => array('cleanup_tool'),
  );
  $items['cleanup_tool-check'] = array(
    'arguments' => array(),
    'description' => 'Show all modules that are orphaned (files not on filesystem and was not pm_uninstalled and still has record in system table.)',
    'examples' => array(
      'drush cu-check' => 'Show / list summary of all modules that are orphaned.',
    ),
    'drupal dependencies' => array('cleanup_tool'),
    'aliases' => array('cu-check'),
  );
  return $items;
}

/**
 * Implementats of hook_drush_help().
 */
function cleanup_tool_drush_help($section) {
  switch ($section) {
    case 'drush:cleanup_tool-module-clean':
      return dt("Remove orphaned module(s) from the system table.  The update_option parameter is required");
    case 'drush:cleanup_tool-check':
      return dt("Show / list summary of all modules that are orphaned.");
  }
}

/**
 * Drush command callback.
 */
function drush_cleanup_tool_clean() {
  if ($args = func_get_args()) {
    $update_option = drush_get_option('update_option') ? drush_get_option('update_option') : FALSE;
    if ($update_option == 'all') {
      drush_cleanup_tool_check();
      drush_print('');
      drush_print(dt("DELETE ALL THE ABOVE ORPHANED RECORDS FROM THE SYSTEM TABLE, to see a list run 'drush cu-check'."));
      $args[0]=$update_option . ' modules';
    } elseif ($update_option != 'simple') {
      drush_print(dt("Please specify an --update_option parameter, the choice is '--update_option=simple' or '--update_option=all'"));
      drush_print(dt("  'all' clean all orphaned modules that have not been properly pm_uninstalled, do this at your own risk."));
      return drush_set_error('', dt('The !update_option option is not valid', array('!update_option' => $update_option)));
    }
    // list the arguments.
    drush_print();
    drush_print('Module to clean  = ' . $args[0]);
    drush_print();
  } else {
    drush_print(dt("Please specify an argument.  Please see: drush help cu-clean or drush help cu-check"));
    return drush_set_error('arguments_invalid', dt('Invalid arguments or arguments not provided, please see drush help cu-clean or drush help cu-check'));
  }
  if (strlen($args[0]) < 1 ) {
    return drush_set_error('arguments_invalid', dt('You must specify a module name or else the word cleanall, please see drush help cleanup_tool-clean'));
  }
  // Determine if -y was supplied. If so, we can filter out needless output
  // from this command.
  $skip_confirmation = drush_get_context('DRUSH_AFFIRMATIVE');
  if (!drush_confirm(dt('Are you sure?'))) {
    return drush_user_abort();
  }
  drush_log(dt('Beginning cleanup_tool-clean operations.'), 'ok');

  //load all rows from system table
  $result = db_query("SELECT * FROM {system}");

  $items = array();
  foreach ($result as $record) {
    $record->info = unserialize($record->info);
    $items[$record->name] = $record;
  }

  $b_success=FALSE;
  foreach ($items as $item) {
    //Loop through all rows in system table, next line checks to see if the file exists

    if ( (!file_exists($item->filename) && $update_option == 'all') || (!file_exists($item->filename) && $item->name == $args[0]) ) {
      drush_print('Deleting the following record from the system table:');
      drush_print('name           = ' . $item->name);
      drush_print('type           = ' . $item->type);
      drush_print('filename       = ' . $item->filename);
      drush_print('Owner          = ' . $item->owner);
      drush_print('Bootstrap      = ' . $item->bootstrap);
      drush_print('schema_version = ' . $item->schema_version);
      drush_print('weight         = ' . $item->weight);
      $b_success = db_delete('system')
                  ->condition('filename', $item->filename)
                  ->execute();
      watchdog('cleanup_tool debug', '<pre>' . 
                              '%item_array' .
                              '</pre> DELETE FROM {system} WHERE filename = ' .
                              $item->filename . " return value=" . $b_success, array('%item_array' => $item));
    }
  }

  if ($b_success) {
    if ($update_option == 'all') {
      drush_cleanup_tool_check();
    }
    drush_print("operation successful, " . $args[0] . " DELETED from system table" );
    return $b_success;
  } else {
    if ($update_option == 'all') {
      drush_cleanup_tool_check();
      return $DRUSH_SUCCESS;
    } else {
      drush_print($args[0] . ' is not an orphaned module');
      drush_print("arguments were:" . $args[0]);
      drush_print("operation failure");
      return drush_set_error('cleanup_tool_clean_operation_failure', dt('parameter = !param  Failure notice, cleanup_tool clean operation run drush help cleanup_tool-clean', array('!param' => $args[0])));
    }
  }

}

/**
 * Get a list of all modules that are orphaned.
 */
function drush_cleanup_tool_check() {
  $query = db_select('system', 's');
  $query->fields('s', array('filename','name','type','owner','status','bootstrap','schema_version','weight' ))
        ->orderBy('type', 'DESC');

  $result = $query->execute();

  $b_clean=TRUE;
    drush_print('');
    drush_print('Check for modules that have not been properly uninstalled');
    drush_print('');
  while($record = $result->fetchObject()) {
    if (!file_exists($record->filename)) {
      if ($b_clean) {
        drush_print('');
        drush_print('_______ modules that have not been properly uninstalled_________');
      }
      $b_clean=FALSE;
      drush_print('');
      drush_print('name           = ' . $record->name);
      drush_print('type           = ' . $record->type);
      drush_print('filename       = ' . $record->filename);
      drush_print('Owner          = ' . $record->owner);
      drush_print('Bootstrap      = ' . $record->bootstrap);
      drush_print('schema_version = ' . $record->schema_version);
      drush_print('weight         = ' . $record->weight);
    }
  }
  if ($b_clean) {
    drush_print('Installation is clean');
  } else {
    drush_print('_______________________________________________________________');
  }
}
