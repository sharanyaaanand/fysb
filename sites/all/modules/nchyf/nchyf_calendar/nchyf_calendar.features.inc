<?php

/**
 * @file
 * nchyf_calendar.features.inc
 */

/**
 * Implements hook_views_api().
 */
function nchyf_calendar_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function nchyf_calendar_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: home_page_calender_events
  $nodequeues['home_page_calender_events'] = array(
    'name' => 'home_page_calender_events',
    'title' => 'Home Page Calender Events',
    'subqueue_title' => '',
    'size' => 8,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 1,
    'insert_at_front' => 1,
    'i18n' => 0,
    'unique_entries' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'calendar',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}
