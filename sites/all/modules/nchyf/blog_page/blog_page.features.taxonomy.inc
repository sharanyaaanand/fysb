<?php

/**
 * @file
 * blog_page.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function blog_page_taxonomy_default_vocabularies() {
  return array(
    'vocabulary_10' => array(
      'name' => 'Blog',
      'machine_name' => 'vocabulary_10',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -4,
    ),
    'vocabulary_2' => array(
      'name' => 'Tags',
      'machine_name' => 'vocabulary_2',
      'description' => 'Tags for multiple content types',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -2,
    ),
    'vocabulary_5' => array(
      'name' => 'Topics',
      'machine_name' => 'vocabulary_5',
      'description' => 'Topic navigation',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -1,
    ),
  );
}
