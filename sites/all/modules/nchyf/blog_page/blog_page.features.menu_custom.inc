<?php

/**
 * @file
 * blog_page.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function blog_page_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-main-menu-2-0.
  $menus['menu-main-menu-2-0'] = array(
    'menu_name' => 'menu-main-menu-2-0',
    'title' => 'Main menu 2.0',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Main menu 2.0');

  return $menus;
}
