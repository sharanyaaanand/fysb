<?php

/**
 * @file
 * blog_page.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function blog_page_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-news_articles-block_1'] = array(
    'cache' => -1,
    'css_class' => 'subpage-aside list-news',
    'custom' => 0,
    'delta' => 'news_articles-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'blog
blog/*',
    'roles' => array(),
    'themes' => array(
      'ncfy_omega' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'ncfy_omega',
        'weight' => 22,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-news_articles-block_2'] = array(
    'cache' => -1,
    'css_class' => 'home-latest-post',
    'custom' => 0,
    'delta' => 'news_articles-block_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'ncfy_omega' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'ncfy_omega',
        'weight' => -59,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['views-news_articles-block_3'] = array(
    'cache' => -1,
    'css_class' => 'subpage-aside archives',
    'custom' => 0,
    'delta' => 'news_articles-block_3',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'blog
blog/*',
    'roles' => array(),
    'themes' => array(
      'ncfy_omega' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'ncfy_omega',
        'weight' => 23,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
