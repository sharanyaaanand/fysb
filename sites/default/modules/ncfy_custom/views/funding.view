<?php
$view = new view();
$view->name = 'funding';
$view->description = '';
$view->tag = 'NCFY Custom';
$view->base_table = 'node';
$view->human_name = 'Funding Opportunities';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['inline'] = array(
  'title' => 'title',
  'field_due_date' => 'field_due_date',
  'taxonomy_vocabulary_3' => 'taxonomy_vocabulary_3',
);
$handler->display->display_options['row_options']['separator'] = '<br>';
$handler->display->display_options['row_options']['hide_empty'] = TRUE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['content'] = '<h1 class="views-title">Funding Opportunities</h1>
NCFY compiles funding opportunities from federal agencies, private foundations and corporations. Please read the full funding opportunity and consult the funder\'s website before deciding to apply. You may also want to read NCFY\'s articles on <a href="../taxonomy/term/10">fundraising</a> and <a href="../taxonomy/term/22">grant writing</a>.';
$handler->display->display_options['header']['area']['format'] = '1';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_type'] = '0';
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
/* Field: Content: Due Date */
$handler->display->display_options['fields']['field_due_date']['id'] = 'field_due_date';
$handler->display->display_options['fields']['field_due_date']['table'] = 'field_data_field_due_date';
$handler->display->display_options['fields']['field_due_date']['field'] = 'field_due_date';
$handler->display->display_options['fields']['field_due_date']['element_type'] = 'span';
$handler->display->display_options['fields']['field_due_date']['settings'] = array(
  'format_type' => 'long',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Content: ShareThis Link */
$handler->display->display_options['fields']['sharethis']['id'] = 'sharethis';
$handler->display->display_options['fields']['sharethis']['table'] = 'node';
$handler->display->display_options['fields']['sharethis']['field'] = 'sharethis';
/* Sort criterion: Content: Sticky */
$handler->display->display_options['sorts']['sticky']['id'] = 'sticky';
$handler->display->display_options['sorts']['sticky']['table'] = 'node';
$handler->display->display_options['sorts']['sticky']['field'] = 'sticky';
$handler->display->display_options['sorts']['sticky']['order'] = 'DESC';
/* Sort criterion: Content: Due Date (field_due_date) */
$handler->display->display_options['sorts']['field_due_date_value']['id'] = 'field_due_date_value';
$handler->display->display_options['sorts']['field_due_date_value']['table'] = 'field_data_field_due_date';
$handler->display->display_options['sorts']['field_due_date_value']['field'] = 'field_due_date_value';
$handler->display->display_options['filter_groups']['groups'] = array(
  1 => 'AND',
  2 => 'OR',
);
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'funding_info' => 'funding_info',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Sticky */
$handler->display->display_options['filters']['sticky']['id'] = 'sticky';
$handler->display->display_options['filters']['sticky']['table'] = 'node';
$handler->display->display_options['filters']['sticky']['field'] = 'sticky';
$handler->display->display_options['filters']['sticky']['value'] = '1';
$handler->display->display_options['filters']['sticky']['group'] = 2;
/* Filter criterion: Content: Due Date (field_due_date) */
$handler->display->display_options['filters']['field_due_date_value']['id'] = 'field_due_date_value';
$handler->display->display_options['filters']['field_due_date_value']['table'] = 'field_data_field_due_date';
$handler->display->display_options['filters']['field_due_date_value']['field'] = 'field_due_date_value';
$handler->display->display_options['filters']['field_due_date_value']['operator'] = '>=';
$handler->display->display_options['filters']['field_due_date_value']['group'] = 2;
$handler->display->display_options['filters']['field_due_date_value']['default_date'] = 'now';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Funding Opportunities';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['inline'] = array(
  'title' => 'title',
  'field_due_date' => 'field_due_date',
);
$handler->display->display_options['row_options']['separator'] = '<br>';
$handler->display->display_options['row_options']['hide_empty'] = TRUE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['content'] = '<h1 class="views-title">Funding Opportunities</h1>
<p>[sharethis]</p>
<p>&nbsp;</p>
<p>NCFY compiles funding opportunities from federal agencies, private foundations and corporations. Please read the full funding opportunity and consult the funder\'s website before deciding to apply. You may also want to read NCFY\'s articles on <a href="../taxonomy/term/10">fundraising</a> and <a href="../taxonomy/term/22">grant writing</a>.</p>';
$handler->display->display_options['header']['area']['format'] = '4';
$handler->display->display_options['header']['area']['tokenize'] = TRUE;
$handler->display->display_options['defaults']['footer'] = FALSE;
/* Footer: Global: Text area */
$handler->display->display_options['footer']['area']['id'] = 'area';
$handler->display->display_options['footer']['area']['table'] = 'views';
$handler->display->display_options['footer']['area']['field'] = 'area';
$handler->display->display_options['footer']['area']['content'] = '<p>The <a href="http://www.acf.hhs.gov/programs/fysb">Family and Youth Services Bureau</a> offers a number of grant programs:</p>
<ul><a href="http://www.findyouthinfo.gov/funding-information-center/grants-search" title="Visit FindYouthInfo.gov!"><img alt="Badge for FindYouthInfo.gov: Grants.gov on Find Youth Info" src="http://www.findyouthinfo.gov/img/grantsGov.jpg" align="right"/></a>
<li><a href="http://www.acf.hhs.gov/programs/fysb/content/programs/fv.htm">Family Violence Prevention Services Programs</a></li>
<li><a href="http://www.acf.hhs.gov/programs/fysb/content/programs/rhy.htm">Runaway and Homeless Youth Programs</a></li>
<li><a href="http://www.acf.hhs.gov/programs/fysb/content/programs/tpp.htm">Adolescent Pregnancy Prevention Programs</a></li>
</ul>
<p>FYSB\'s open funding announcements are posted here on the NCFY website, on <a href="http://www.grants.gov/">Grants.gov</a> and on the <a href="http://www.acf.hhs.gov/grants/open/foa/">ACF Funding Opportunities</a> page. Feel free to <a href="/node/19">contact us</a> for more information.</p>';
$handler->display->display_options['footer']['area']['format'] = '4';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['exclude'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_type'] = '0';
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
/* Field: Content: Due Date */
$handler->display->display_options['fields']['field_due_date']['id'] = 'field_due_date';
$handler->display->display_options['fields']['field_due_date']['table'] = 'field_data_field_due_date';
$handler->display->display_options['fields']['field_due_date']['field'] = 'field_due_date';
$handler->display->display_options['fields']['field_due_date']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_due_date']['element_type'] = 'span';
$handler->display->display_options['fields']['field_due_date']['settings'] = array(
  'format_type' => 'long',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = '';
$handler->display->display_options['fields']['nothing']['alter']['text'] = '<p><strong>[title]</strong><br><strong>Due Date:</strong> [field_due_date]</p>';
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
/* Field: Content: ShareThis Link */
$handler->display->display_options['fields']['sharethis']['id'] = 'sharethis';
$handler->display->display_options['fields']['sharethis']['table'] = 'node';
$handler->display->display_options['fields']['sharethis']['field'] = 'sharethis';
$handler->display->display_options['fields']['sharethis']['label'] = '';
$handler->display->display_options['fields']['sharethis']['exclude'] = TRUE;
$handler->display->display_options['fields']['sharethis']['element_label_colon'] = FALSE;
$handler->display->display_options['path'] = 'funding';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Funding';
$handler->display->display_options['menu']['weight'] = '3';
$handler->display->display_options['menu']['name'] = 'main-menu';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: Feed */
$handler = $view->new_display('feed', 'Feed', 'feed');
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['style_plugin'] = 'rss';
$handler->display->display_options['row_plugin'] = 'node_rss';
$handler->display->display_options['path'] = 'funding/feed';
$handler->display->display_options['displays'] = array(
  'default' => 'default',
  'page' => 'page',
);

/* Display: Block */
$handler = $view->new_display('block', 'Block', 'block_1');
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['content'] = '<h1 class="views-title">Funding Opportunities</h1>
NCFY compiles funding opportunities from federal agencies, private foundations and corporations. Please read the full funding opportunity and consult the funder\'s website before deciding to apply. You may also want to read NCFY\'s articles on <a href="../taxonomy/term/10">fundraising</a> and <a href="../taxonomy/term/22">grant writing</a>.';
$handler->display->display_options['header']['area']['format'] = '1';

