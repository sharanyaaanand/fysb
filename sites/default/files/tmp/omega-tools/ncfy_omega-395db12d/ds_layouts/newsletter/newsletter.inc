<?php
function ds_newsletter() {
  return array(
    'label' => t('Newsletter'),
    'regions' => array(
      'featuretext' => t('Feature Text'),
      'featureimage' => t('Feature Image'),
      'newsarea' => t('News Area'),
      'funding' => t('Funding Opportunities'),
      'newsletterurl' => t('Newsletter URL')
    ),
    // Add this line if there is a default css file.
    'css' => TRUE,
    // Add this line if you're using DS 2.x for icon preview
    'image' => TRUE,
  );
}
?>